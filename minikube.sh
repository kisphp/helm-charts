#!/bin/bash
# shellcheck disable=SC2045

for CHART in $(ls charts/)
do
  helm upgrade --install -n kisphp "${CHART}" "charts/${CHART}"
done
