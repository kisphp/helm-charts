.PHONY: index docker

test:
	helm repo rm kisphp-helm-charts
	python3 deploy.py

docker:
	docker build -f docker/Dockerfile -t my-docker-helm .
	docker run --rm -w /app -it my-docker-helm bash

index:
	helm package mychart
	mv mychart-* public/
	helm repo index public --url https://localhost:8000/

use:
	helm repo add kisphp-helm-charts https://gitlab.com/api/v4/projects/32045261/packages/helm/stable
	helm repo ls

tpl:
	rm -rf _manifests
	#helm template --output-dir _manifests/backend backend charts/default-backend
	#helm template --output-dir _manifests/redis --debug redis charts/redis
	helm template --output-dir _manifests/mysql --debug mysql charts/mysql

up:
	helm upgrade -n default --install mysql charts/mysql
