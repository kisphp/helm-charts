Add helm repo

```bash
helm repo add kisphp-helm-charts https://gitlab.com/api/v4/projects/32045261/packages/helm/stable
```

Install chart from the repository

```bash
helm install --namespace kisphp mysql kisphp-helm-charts/mysql
```

Install local chart

```bash
helm upgrade --install --namespace kisphp mysql ./mysql
```


## Deploy all on minikube

```bash
source minikube.sh
```
