## FluxCD setup

```yaml
apiVersion: source.toolkit.fluxcd.io/v1
kind: GitRepository
metadata:
  name: kisphp-charts
  namespace: flux-system
spec:
  interval: 1m
  url: https://gitlab.com/kisphp/helm-charts
  ref:
    branch: main
---
apiVersion: helm.toolkit.fluxcd.io/v2
kind: HelmRelease
metadata:
  name: postgres
  namespace: default
spec:
  interval: 1m
  driftDetection:
    mode: warn
  chart:
    spec:
      chart: ./charts/postgres
      version: '*'
      sourceRef:
        kind: GitRepository
        name: kisphp-charts
        namespace: flux-system
      interval: 1m
  values:
    configMapsData:
      POSTGRES_DB: kisphp
      POSTGRES_PASSWORD: kisphp
```
