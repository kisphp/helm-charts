#!/usr/bin/env python3

import requests
import yaml
import os
import sys


class ChartManager:
    def __init__(self, is_main_branch):
        project_id = os.getenv('CI_PROJECT_ID')
        self.URL = f"https://gitlab.com/api/v4/projects/{project_id}/packages/helm/stable"
        self.POST_URL = f"https://gitlab.com/api/v4/projects/{project_id}/packages/helm/api/stable/charts"

        self.execute_file = 'execute.sh'
        self.is_main = is_main_branch
        self.versions = self.get_all_charts_versions()
        self.username = 'gitlab-ci-token'
        self.password = os.getenv('CI_JOB_TOKEN')
        self.repo_name = 'kisphp-helm-charts'

        with open(self.execute_file, 'w') as file:
            file.write("#!/bin/bash\n\n")

    @staticmethod
    def get_chart_info(chart_path):
        with open(f'charts/{chart_path}/Chart.yaml', 'r') as file:
            file_content = yaml.safe_load(file.read())

            return file_content

    def init_repo(self):
        cmd = f"helm repo add --username {self.username} --password {self.password} {self.repo_name} {self.URL}"
        self.run_cmd(cmd)

    def get_all_charts_versions(self):
        print(f'Helm index: {self.URL}/index.yaml')
        payload = requests.get(f'{self.URL}/index.yaml')
        content = yaml.safe_load(payload.text)

        versions = {}
        for item in content['entries']:
            for version in content['entries'][item]:
                if item in versions:
                    versions.get(item).append(version['version'])
                else:
                    versions.update({
                        item: [version['version']]
                    })

        return versions

    def create_chart(self, chart_path):
        print('-'*100)
        print(f"Generating chart: {chart_path}")
        chart_data = self.get_chart_info(chart_path)
        chart_name = chart_data['name']
        current_version = chart_data['version']

        if chart_name not in self.versions:
            self.versions.update({
                chart_name: []
            })

        if current_version in self.versions[chart_name]:
            print(f'Version {current_version} exists for {chart_name}, skipping...')
            return None

        print(f'Version {current_version} does not exists for {chart_name}, building...')
        self.generate_and_push_chart(chart_name, current_version)

    def generate_and_push_chart(self, chart_name, current_version):
        out, err = self.run_cmd(f'helm package charts/{chart_name}')
        if err is None:
            if self.is_main:
                cmd = f"helm cm-push {chart_name}-{current_version}.tgz {self.repo_name}"
                print(f"Publishing {chart_name} version {current_version}")
                self.run_cmd(cmd)
            else:
                print(f"{chart_name} version {current_version} will be published when the branch will be merged")
                cmd = f"helm template {chart_name} charts/{chart_name}"
                self.run_cmd(cmd)

    @staticmethod
    def run_cmd(command):
        print(f"Running: {command}")

        with open('execute.sh', 'a') as file:
            file.write(f"{command}\n")

        return True, None


if __name__ == '__main__':
    charts = os.listdir('charts')

    is_main_branch = False
    if len(sys.argv) > 1 and (sys.argv[1] == 'main'):
        is_main_branch = True

    cm = ChartManager(is_main_branch)
    cm.init_repo()

    for chart in charts:
        cm.create_chart(chart)
